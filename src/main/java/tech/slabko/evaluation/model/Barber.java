package tech.slabko.evaluation.model;

import javax.persistence.*;

@Entity
public class Barber {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    private String firstName;
    private String lastName;
}
